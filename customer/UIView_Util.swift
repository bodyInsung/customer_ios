//
//  UIView_Util.swift
//  customer
//
//  Created by insung on 2019/11/18.
//  Copyright © 2019 bodyfriend. All rights reserved.
//

import UIKit
import SnapKit

extension UIView {

    func applyAutoLayoutFromSuperview(_ superview: UIView) {
        self.snp.makeConstraints({ (make) in
            make.top.bottom.leading.trailing.equalTo(superview);
        })
    }
}
