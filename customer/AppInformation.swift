//
//  AppInformation.swift
//  customer
//
//  Created by insung on 2019/11/18.
//  Copyright © 2019 bodyfriend. All rights reserved.
//

import Foundation

struct AppInformation: Codable {
    let results: [AppInfo]
    let resultCount: Int
}

struct AppInfo: Codable {
    let version: String
}
