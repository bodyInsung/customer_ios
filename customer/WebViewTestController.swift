//
//  WebViewTestController.swift
//  customer4
//
//  Created by bodyfriend_dev on 2017. 11. 3..
//  Copyright © 2017년 bodyfriend. All rights reserved.
//

import UIKit
class WebViewTestController: UIViewController , UIWebViewDelegate{
    
    @IBOutlet var webview: UIWebView!
    
    let url = "http://ctest.bodyfriend.co.kr/login/login.view"
    
    override func loadView() {
        super.loadView()
        
        webview.delegate = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let request = URLRequest(url: URL(string: url)!)
        webview.loadRequest(request)
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        
        print("startUrl : \(request.url!.absoluteString)")
        
        
        return true
    }
    
}
