//
//  SplashViewController.swift
//  customer
//
//  Created by ios on 2018. 5. 17..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit
import Alamofire

class SplashViewController: UIViewController {
    @IBOutlet var logoIv: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLayout()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func setLayout() -> Void {
        animateLogo()
    }
    
    func animateLogo() -> Void {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
            self.requestVersion()
        })
    }
    
    func requestVersion() {
        let infoDic = Bundle.main.infoDictionary
        let appId = infoDic?["CFBundleIdentifier"] as! String
        let url = String(format: Constants.URL.APP_STORE_INFO, appId)
        print("request url :", url)
        AF.request(url, method: .post).responseJSON { (response) in
            do {
                if let responseData = response.data {
                    let decode = try JSONDecoder().decode(AppInformation.self, from: responseData)
                    if let result = decode.results.first {
                        let dataVersions = result.version.split(separator: ".")
                        
                        let bundleVersion = infoDic?["CFBundleShortVersionString"] as! String
                        let versions = bundleVersion.split(separator: ".")
                        
                        if let major = Int(versions[0]),
                            let dataMajor = Int(dataVersions[0]) {
                            if major < dataMajor {
                                self.showPopup(.major)
                                return
                            }
                        }
                        
                        if let minor = Int(versions[1]),
                            let dataMinor = Int(dataVersions[1]) {
                            if minor < dataMinor {
                                self.showPopup(.minor)
                                return
                            }
                        }
                        
                        if let bug = Int(versions.indices ~= 2 ? versions[2] : ""),
                            let dataBug = Int(dataVersions.indices ~= 2 ? dataVersions[2] : "") {
                            if bug < dataBug {
                                self.showPopup(.minor)
                                return
                            }
                        }
                        self.moveMainViewController()
                    }
                }
            } catch (let error) {
                self.showPopup(.warning)
            }
        }
    }
    
    func showPopup(_ type: PopupType) {
        if type == .major {
            CommonUtil.showAlert(title: "앱 업데이트", msg: "업데이트가 필요합니다", target: self, cancel: nil, confirm: {
                CommonUtil.openAppStore()
            })
        } else if type == .minor {
            CommonUtil.showAlert(title: "앱 업데이트", msg: "업데이트가 있습니다", target: self, cancel: {
                self.moveMainViewController()
            }, confirm: {
                CommonUtil.openAppStore()
            })
        } else {
            CommonUtil.showAlert(title: "", msg: "통신이 원활하지 않습니다", target: self, cancel: nil, confirm: {
                exit(0)
            })
        }
    }
    
    func moveMainViewController() {
        let mainSb = UIStoryboard(name: Constants.Identifier.STORYBOARD_MAIN_NAME, bundle: nil)
        let mainVC = mainSb.instantiateViewController(withIdentifier: Constants.Identifier.STORYBOARD_MAIN) as! MainViewController
        Constants.Define.APPDELEGATE.window?.rootViewController = mainVC
        
    }
}
