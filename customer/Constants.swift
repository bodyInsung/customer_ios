//
//  Constants.swift
//  customer
//
//  Created by insung on 2018. 7. 3..
//  Copyright © 2018년 bodyfriend. All rights reserved.
//

import UIKit

struct Constants {
    struct URL {
        #if DEBUG
//        static let BASE = "http://121.138.34.30:8080/"
        static let BASE = "http://172.30.40.35:8080/"           // 최대리님
//        static let BASE = "http://121.138.34.217:8081"          // 최대리님
        #else
        static let BASE = "https://c.bodyfriend.co.kr"
        #endif
        
        static let APP_VERSION = "http://www.bodyfriend.co.kr/api/common/getAppVersionInfo?appCd=C&osCd=IPHONE"
        static let APP_STORE = "https://itunes.apple.com/us/app/id1329457954?l=ko&ls=1&mt=8"
        static let APP_STORE_INFO = "http://itunes.apple.com/lookup?bundleId=%@"
    }
    struct Define {
        static let APPDELEGATE = UIApplication.shared.delegate as! AppDelegate
    }
    
    struct Identifier {
        static let STORYBOARD_MAIN_NAME = "Main"
        static let STORYBOARD_MAIN = "MainSID"
    }
}

enum PopupType {
    case major
    case minor
    case warning
}

