//
//  ViewController.swift
//  customer4
//
//  Created by bodyfriend_dev on 2017. 10. 25..
//  Copyright © 2017년 bodyfriend. All rights reserved.
//

import UIKit
import WebKit
import Firebase

class MainViewController: UIViewController, WKNavigationDelegate, WKUIDelegate, WKScriptMessageHandler {
    
    @IBOutlet var mainContentV: UIView!
    @IBOutlet var subBackV: UIView!
    @IBOutlet var subContentV: UIView!
    @IBOutlet weak var loadingV: UIView!
    
    var mainWV: WKWebView!
    var subWV: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initLayout()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        mainWV.frame = mainContentV.bounds
    }
    
    func initLayout() {
        loadingV.isHidden = true
        
//        let configuration = WKWebViewConfiguration()
//        configuration.allowsInlineMediaPlayback = true
//        configuration.preferences.javaScriptEnabled = true
//        configuration.preferences.javaScriptCanOpenWindowsAutomatically = true
//        // 쿠키 항상 허용 (모바일 인증시 필요)
//        configuration.processPool.perform(NSSelectorFromString("_setCookieAcceptPolicy:"), with: HTTPCookie.AcceptPolicy.always)
//
//        configuration.userContentController.add(self, name: "notificationBridge")

        loadWebPage(Constants.URL.BASE)
        checkPushData()
    }
    
    func loadWebPage(_ url: String) {
        let userContentController = WKUserContentController()
        if let cookies = HTTPCookieStorage.shared.cookies {
            let script = getJSCookiesString(for: cookies)
            let cookieScript = WKUserScript(source: script, injectionTime: .atDocumentStart, forMainFrameOnly: false)
            userContentController.addUserScript(cookieScript)
        }
        let webViewConfig = WKWebViewConfiguration()
        webViewConfig.userContentController = userContentController
        webViewConfig.userContentController.add(self, name: "notificationBridge")

        mainWV = WKWebView(frame: mainContentV.bounds, configuration: webViewConfig)
        mainWV.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        mainWV.backgroundColor = UIColor.white
        mainContentV.addSubview(mainWV)
        mainWV.uiDelegate = self
        mainWV.navigationDelegate = self
        
        let myurl = URL(string:url)
        var request = URLRequest(url: myurl!)
        request.httpShouldHandleCookies = true
        mainWV.load(request)
    }

    func getJSCookiesString(for cookies: [HTTPCookie]) -> String {
        var result = ""
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "EEE, d MMM yyyy HH:mm:ss zzz"

        for cookie in cookies {
            result += "document.cookie='\(cookie.name)=\(cookie.value); domain=\(cookie.domain); path=\(cookie.path); "
            if let date = cookie.expiresDate {
                result += "expires=\(dateFormatter.string(from: date)); "
            }
            if (cookie.isSecure) {
                result += "secure; "
            }
            result += "'; "
        }
        return result
    }
    
    func checkPushData() {
        if let pushData = Constants.Define.APPDELEGATE.pushData {
            CommonUtil.reloadPushData(pushData)
            Constants.Define.APPDELEGATE.pushData = nil
        }
    }
    
    func loadingForDelay() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.loadingV.isHidden = true
        }
    }
    
    @objc func notificationBridge(_ values: [String: Any]) {
        if let topics = values["topics"] as? [String] {
            for topic in topics {
                if let notiYn = values["notiYn"] as? Bool, notiYn == true {
                    Messaging.messaging().subscribe(toTopic: topic)
                } else {
                    Messaging.messaging().unsubscribe(fromTopic: topic)
                }
            }
        }
    }
    
    // MARK: Action Event
    @IBAction func pressedClose(_ sender: UIButton) {
        webViewDidClose(subWV)
    }
    
    // MARK: WKWebViewDelegate
    public func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction:
        WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
        print("createWebViewWith")
        subWV = WKWebView(frame: subContentV.bounds, configuration: configuration)
        subWV.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        subWV.uiDelegate = self
        subWV.navigationDelegate = self
        subContentV.addSubview(subWV!)
        subBackV.isHidden = false
        return subWV!
    }
    
    /// 웹뷰가 종료 되었을때
    ///
    /// - Parameter webView: webView
    func webViewDidClose(_ webView: WKWebView) {
        subBackV.isHidden = true
        if webView == subWV {
            subWV?.removeFromSuperview()
            subWV = nil
        }
    }

    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        print("didCommit :", webView.url?.absoluteString as Any)
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
//        loadingV.isHidden = false
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
//        loadingForDelay()
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
//        loadingForDelay()
    }
    
    func webView(_ webView: WKWebView, commitPreviewingViewController previewingViewController: UIViewController) {
        print("commitPreviewingViewController")
    }
    
    func webView(_ webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!) {
        print("didReceiveServerRedirectForProvisionalNavigation")
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print("didFailProvisionalNavigation")
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        let url = navigationAction.request.url!
        print("decidePolicyFor :", url)
        if url.scheme == "about" {
            decisionHandler(.cancel)
        } else if url.scheme != "http" && url.scheme != "https" {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
            decisionHandler(.cancel)
        } else {
            decisionHandler(.allow)
        }
    }
    
    /// 알럿 컨트롤
    ///
    /// - Parameters:
    ///   - webView: webView
    ///   - message: message
    ///   - frame: frame
    ///   - completionHandler: completionHandler
    func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping () -> Void) {
        // 메시지창 컨트롤러 인스턴스를 생성한다
        CommonUtil.showAlert(title: "알림", msg: message, target: self, cancel: nil) {
            completionHandler()
        }
    }
    
    /// 자바스크립트 컨펌 컨트롤
    ///
    /// - Parameters:
    ///   - webView: webView
    ///   - message: message
    ///   - frame: frame
    ///   - completionHandler: completionHandler
    func webView(_ webView: WKWebView, runJavaScriptConfirmPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping (Bool) -> Void) {
        CommonUtil.showAlert(title: "알림", msg: message, target: self, cancel: {
            completionHandler(false)
        }) {
            completionHandler(true)
        }
    }
 
    func webView(_ webView: WKWebView, runJavaScriptTextInputPanelWithPrompt prompt: String, defaultText: String?, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping (String?) -> Void) {
        print("runJavaScriptTextInputPanelWithPrompt")
        completionHandler(defaultText)
    }
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        let values: [String: Any] = message.body as! Dictionary
        if(message.name == "notificationBridge") {
            perform(#selector(notificationBridge(_:)), with: values)
        }
    }
}
